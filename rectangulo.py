class Rectangulo:
    def __init__(self, n, b, h):
        self.name = n
        self.base = b
        self.altura = h

    def calcularArea(self):
        return self.base * self.altura
    
    def calcularPerimetro(self):
        return self.base*2 + self.altura*2
    
    def __str__(self):
        return "El area del {name} es {area} y el perimetro es {perim}".format(name=self.name, area=self.calcularArea(), perim=self.calcularPerimetro())

rectangulo1 = Rectangulo('Rectángulo 1', 3, 6)
rectangulo2 = Rectangulo('Rectángulo 2', 5, 8)

print(rectangulo1)
print(rectangulo2)