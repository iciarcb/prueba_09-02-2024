import rectangulo
import unittest

class TestRectangulo(unittest.TestCase):
    def test_perimetro(self):
        rectangulo1 = rectangulo('1', 1, 2)
        perimetro1 = rectangulo1.calcularPerimetro()
        self.assertNotEqual(perimetro1, 25)
    def test_area(self):
        rectangulo2 = rectangulo('2', 3, 4)
        area2 = rectangulo2.calcularArea()
        self.assertNotEqual(area2, 14)

unittest.main()