class Cuadrado:
    def __init__(self, l, n):
        self.name = n
        self.lado = l
    
    def calculoPerimetro(self):
        return self.lado * 4

    def calculoArea(self):
        return self.lado * self.lado
    def __str__(self):
        return "El area del {name} es {area} y el perimetro es {perim}".format(name=self.name, area=self.calculoArea(), perim=self.calculoPerimetro())

cuadradoGrande = Cuadrado(3, "Grande")
cuadradoPequeño = Cuadrado(8, "Pequeño")

print(cuadradoGrande.calculoPerimetro)
print(cuadradoPequeño.calculoArea)
